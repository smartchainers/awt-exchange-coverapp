import ApiClient from "@zsmartex/z-apiclient";
import * as helpers from "@zsmartex/z-helpers";
import { ActionTree } from "vuex";

const actions: ActionTree<ExchangeState, RootState> = {
};

export default actions;
